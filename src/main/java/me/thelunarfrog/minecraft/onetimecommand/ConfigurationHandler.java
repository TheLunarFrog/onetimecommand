package main.java.me.thelunarfrog.minecraft.onetimecommand;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

/**
 * Handles the OneTimeCommand configuration.
 * @version 1.0.0.0
 * @category Configuration
 * @since 1.0.0.0
 * @author TheLunarFrog
 */
@SuppressWarnings({ "unchecked", "unused" })
public class ConfigurationHandler extends OneTimeCommand {

	protected static YamlConfiguration Settings;
	protected static File configFile;
	private static boolean loaded = false;
	private static OneTimeCommand fa = new OneTimeCommand();
	OneTimeCommand plugin;

	@Override
	public void saveConfig(){
		try{
			Settings.save(configFile);
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	protected static void loadConfig() throws InvalidConfigurationException{
		configFile = new File(Bukkit.getServer().getPluginManager().getPlugin("OneTimeCommand").getDataFolder(), "config.yml");
		if(configFile.exists()) {
			Settings = new YamlConfiguration();
			try{
				Settings.load(configFile);
				theCommand = Settings.getString("Command", "kit tools");
				playersUsed = (List<String>) Settings.getList("UsedCommand", new ArrayList<String>());
			} catch(FileNotFoundException ex){
				fa.severe("An exception has occurred while OneTimeCommand was loading the configuration.");
				ex.printStackTrace();
			} catch(IOException ex){
				fa.severe("An exception has occurred while OneTimeCommand was loading the configuration.");
				ex.printStackTrace();
			} catch(InvalidConfigurationException ex){
				fa.severe("An exception has occurred while OneTimeCommand was loading the configuration.");
				ex.printStackTrace();
			}
			loaded = true;
		}else{
			try{
				Bukkit.getServer().getPluginManager().getPlugin("OneTimeCommand").getDataFolder().mkdir();
				InputStream jarURL = ConfigurationHandler.class.getResourceAsStream("/main/resources/config.yml");
				copyFile(jarURL, configFile);
				Settings = new YamlConfiguration();
				Settings.load(configFile);
				loaded = true;
				fa.info("Configuration loaded successfully.");
			}catch(Exception e){
				fa.severe("Exception occurred while creating a new configuration file!");
				e.printStackTrace();
			}
		}
	}
	static private void copyFile(InputStream in, File out) throws Exception {
		InputStream fis = in;
		FileOutputStream fos = new FileOutputStream(out);
		try {
			byte[] buf = new byte[1024];
			int i = 0;
			while ((i = fis.read(buf)) != -1) {
				fos.write(buf, 0, i);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (fis != null) {
				fis.close();
			}
			if (fos != null) {
				fos.close();
			}
		}
	}
	protected ConfigurationHandler() {
		this.plugin = OneTimeCommand.plugin;
	}
}