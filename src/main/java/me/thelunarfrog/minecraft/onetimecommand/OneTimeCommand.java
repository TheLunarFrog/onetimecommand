package main.java.me.thelunarfrog.minecraft.onetimecommand;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class OneTimeCommand extends JavaPlugin{
	Logger logger = Logger.getLogger("Minecraft");
	static OneTimeCommand plugin;
	static List<String> playersUsed;
	static String theCommand;
	protected PluginDescriptionFile pdf;
	@Override
	public void onEnable(){
		pdf = this.getDescription();
		try {
			ConfigurationHandler.loadConfig();
		} catch (InvalidConfigurationException e) {
			e.printStackTrace();
		}
		Bukkit.getServer().getPluginManager().registerEvents(new CommandListener(), this);
	}
	@Override
	public void onDisable(){
		info("One-Time Command "+pdf.getVersion()+" by TheLunarFrog has been disabled.");
	}
	public void info(String s){
		logger.log(Level.INFO, "[OTC] "+s);
	}
	public void warning(String s){
		logger.log(Level.WARNING, "[OTC] "+s);
	}
	public void severe(String s){
		logger.log(Level.SEVERE, "[OTC] "+s);
	}
	class CommandListener extends OneTimeCommand implements Listener{
		@EventHandler(priority=EventPriority.MONITOR)
		public void onCommandEvent(PlayerCommandPreprocessEvent evt){
			if(!evt.isCancelled()){
				String cmd = evt.getMessage();
				if(cmd.equalsIgnoreCase(theCommand)){
					Player player = evt.getPlayer();
					if(playersUsed.contains(player.getName())){
						evt.setCancelled(true);
						player.sendMessage(ChatColor.DARK_GREEN+"[OTC] "+ChatColor.RED+"Sorry, but you can only use that command (/"+theCommand+") once!");
					}else{
						player.sendMessage(ChatColor.DARK_GREEN+"[OTC] "+ChatColor.GRAY+"You have used the command, /"+theCommand+", and cannot use it again!");
						playersUsed.add(player.getName());
						ConfigurationHandler.Settings.set("UsedCommand", playersUsed);
						try {
							ConfigurationHandler.Settings.save(ConfigurationHandler.configFile);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
}